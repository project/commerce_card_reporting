<?php

/**
 * @file
 * Views integration for commerce card reporting.
 */

/**
 * Implements hook_views_data_alter().
 */
function commerce_card_reporting_views_data_alter(array &$data) {
  $data['commerce_order_report']['total_completed_transactions'] = [
    'title' => t('Total Completed Transactions'),
    'field' => [
      'title' => t('Total Completed Transactions'),
      'help' => t('Displays the total completed transactions.'),
      'id' => 'commerce_card_reporting_total_completed_transactions',
    ],
  ];
  $data['commerce_order_report']['total_failed_transactions'] = [
    'title' => t('Total Failed Transactions'),
    'field' => [
      'title' => t('Total Failed Transactions'),
      'help' => t('Displays the total failed transactions.'),
      'id' => 'commerce_card_reporting_total_failed_transactions',
    ],
  ];
  $data['commerce_order_report']['total_credit_card_transaction'] = [
    'title' => t('Total Card Transactions'),
    'help' => t('Displays the total card transactions.'),
    'area' => [
      'id' => 'commerce_card_transactions_total',
    ],
  ];
  $data['commerce_order_report']['commerce_card_total_failed_transactions'] = [
    'title' => t('Total Failed Card Transactions'),
    'help' => t('Displays the total failed card transactions number and percentage .'),
    'area' => [
      'id' => 'commerce_card_total_failed_transactions',
    ],
  ];
  $data['commerce_payment']['commerce_card_total_success_transactions'] = [
    'title' => t('Total Successful Card Transactions'),
    'help' => t('Displays the total Successful card transactions number and percentage .'),
    'area' => [
      'id' => 'commerce_card_total_successful_transactions',
    ],
  ];
}
