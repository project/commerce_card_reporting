var gulp = require('gulp');
var sass = require('gulp-sass');
var util = require('gulp-util');
var gulpif = require('gulp-if');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var watch = require('gulp-watch');

var config = {
  assetsDir: '.',
  sassPattern: 'sass/{,*/}*.{scss,sass}',
  production: !!util.env.production,
};

// Compile the CSS etc.
gulp.task('sass', function () {
  'use strict';
  return gulp.src(config.assetsDir+'/'+config.sassPattern)
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(postcss([autoprefixer()]))
    .pipe(rename('credit-card-report-view.css'))
    .pipe(gulp.dest('./css'));
});

// Watch for Sass file changes.
gulp.task('watch', function () {
  'use strict';
  return gulp.watch(config.assetsDir+'/'+config.sassPattern, gulp.series('sass'));
});

// Default task.
// Ensure the CSS is compiled and then watch for file changes.
gulp.task('default', gulp.series('sass', 'watch'));
