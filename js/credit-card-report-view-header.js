/**
 * Credit Card Report View Header.
 */
(function ($, Drupal) {

  Drupal.behaviors.creditCardReportViewHeader = {
    attach: function (context, settings) {
      let from_date = settings.commerce_card_reporting.from_date;
      if (from_date) {
        $('.view-filters__header__date').text(Drupal.t('As of ') + from_date);
      }
    }
  };

})(jQuery, Drupal);
