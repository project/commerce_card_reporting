/**
 * Credit Card Report View.
 */
(function ($, Drupal) {

  Drupal.behaviors.creditCardReportView = {
    attach: function (context, settings) {

      // View Row Details.
      $('.js-ccc-view-row-details').once('creditCardReportView').click(function (e) {
        var $row = $(this).parents('.ccc-card-report-row');

        // Set active class.
        $row.toggleClass('active');

        e.preventDefault();
      });

      // Report List - View All Button.
      $('.js-ccc-card-report__list__view-all-button').once('creditCardReportView').click(function (e) {
        var $list = $(this).parents('ul');

        // Set active class.
        $list.toggleClass('active');

        e.preventDefault();
      });

    }
  };

})(jQuery, Drupal);
