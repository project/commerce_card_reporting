<?php

namespace Drupal\commerce_card_reporting;

use Drupal\commerce_reports\OrderReportGenerator;

/**
 * Generates order reports for orders.
 */
class CardReportGenerator extends OrderReportGenerator {

  public function generateCardReports(
    array $order_ids,
    array $error_details
  ) {
    $orders = $this->orderStorage->loadMultiple($order_ids);
    $plugin_type = $this->reportTypeManager->getDefinitions()['card_report'];
    $generated = 0;

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    foreach ($orders as $order) {
      /** @var \Drupal\commerce_reports\Plugin\Commerce\ReportType\ReportTypeInterface $instance */
      $instance = $this->reportTypeManager->createInstance($plugin_type['id'], []);
      $instance->generateCardReports($order, $error_details);
    }

    return $generated;
  }

  /**
   * {@inheritdoc}
   */
  public function generateReports(array $order_ids, $plugin_id = NULL) {
    $orders = $this->orderStorage->loadMultiple($order_ids);
    $plugin_types = $this->reportTypeManager->getDefinitions();
    $generated = 0;
    // Generate reports for a single report type.
    if ($plugin_id) {
      if (!isset($plugin_types[$plugin_id])) {
        return $generated;
      }
      $plugin_types = [$plugin_types[$plugin_id]];
    }

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    foreach ($orders as $order) {

      foreach ($plugin_types as $plugin_type) {
        /** @var \Drupal\commerce_reports\Plugin\Commerce\ReportType\ReportTypeInterface $instance */
        $instance = $this->reportTypeManager->createInstance($plugin_type['id'], []);
        $instance->generateReports($order);
      }
      $generated++;
    }
    return $generated;
  }

}
