<?php

namespace Drupal\commerce_card_reporting;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Helper class for card reporting.
 */
class CardReportManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new card report manager instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Provides the total number of completed card transactions.
   *
   * @param string $from_date
   *   The report form date.
   * @param string $to_date
   *   The report to date.
   * @param string|null $card_type
   *   The card type to report on.
   */
  public function totalCompletedTransactions(
    $from_date,
    $to_date,
    $card_type = NULL
  ) {
    $query = $this->entityTypeManager
      ->getStorage('commerce_payment')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('state', 'completed')
      ->condition('payment_method.entity.type', 'credit_card');
    if ($from_date) {
      $query->condition('payment_method.entity.created', strtotime($from_date), ">=");
    }

    if ($card_type) {
      $query->condition('payment_method.entity.card_type', $card_type);
    }

    if ($to_date) {
      $query->condition('payment_method.entity.created', strtotime($to_date), "<=");
    }
    return $query->count()->execute();
  }

  /**
   * Provides the total number of completed card transactions.
   *
   * @param string $from_date
   *   The report form date.
   * @param string $to_date
   *   The report to date.
   */
  public function totalFailedTransactions($from_date, $to_date) {
    $query = $this->entityTypeManager
      ->getStorage('commerce_order_report')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'card_report');
    if ($from_date) {
      $query->condition('created', strtotime($from_date), ">=");
    }

    if ($to_date) {
      $query->condition('created', strtotime($to_date), "<=");
    }
    return $query->count()->execute();
  }

  /**
   * Provides the total number of completed card transactions.
   *
   * @param string $from_date
   *   The report form date.
   * @param string $to_date
   *   The report to date.
   */
  public function totalCardTransactions($from_date, $to_date) {
    return $this->totalCompletedTransactions($from_date, $to_date) + $this->totalFailedTransactions($from_date, $to_date);
  }

  /**
   * Provides a percentage of transactions done by a card.
   *
   * @param int $card_transactions
   *   The number of transactions with which the percentage is to be calculated.
   * @param string $from_date
   *   The report form date.
   * @param string $to_date
   *   The report to date.
   *
   * @return string
   *   The percentage of transactions.
   */
  public function getTotalTransactionPercentage(
    $card_transactions,
    $from_date,
    $to_date
  ) {
    $total_card_transactions = $this->totalCardTransactions($from_date, $to_date);

    $percentage = 0;
    if ($total_card_transactions) {
      $percentage = (int) $card_transactions / (int) $total_card_transactions;
    }

    return $percentage * 100;
  }

}
