<?php

namespace Drupal\commerce_card_reporting\EventSubscriber;

use Drupal\commerce_payment\Event\PaymentExceptionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PaymentExceptionEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_payment.payment_gateway.exception' => 'onPaymentException',
    ];
    return $events;
  }

  /**
   * Places the order after it has been fully paid through an off-site gateway.
   *
   * Off-site payments can only be made at checkout.
   * If the gateway supports notifications, these two scenarios are possible:
   *
   * 1) The onNotify() method is called before the customer returns to the
   *    site. A payment is created, the order is now considered fully paid,
   *    causing the "payment" step to no longer be visible, sending the
   *    customer back to the first checkout step.
   * 2) The customer never returns to the site. The onNotify() method completed
   *    the payment, but the order is still unplaced and stuck in checkout.
   *
   * To avoid both problems, this subscriber ensures that the order is placed,
   * which also ensures that the customer is sent to the checkout complete
   * page once they (eventually) return.
   *
   * @param \Drupal\commerce_payment\Event\PaymentExceptionEvent $event
   *   The event.
   */
  public function onPaymentException(PaymentExceptionEvent $event) {
    $commerce_order = \Drupal::routeMatch()
      ->getParameter('commerce_order');
    \Drupal::service('commerce_card_reporting.card_report_generator')
      ->generateCardReports([$commerce_order->id()], [
        'code' => '100',
        'code_label' => 'cvv mismatch',
        'code_description' => 'CVV mistmatch',
        'card_type' => 'visa',
      ]);
  }

}
