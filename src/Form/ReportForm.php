<?php

namespace Drupal\commerce_card_reporting\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the report form.
 */
class ReportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_card_reporting_generate_report_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Your report will be generated based on the parameters set by display filters currently selected on this page.'),
    ];

    $form['pdf_report'] = [
      '#type' => 'button',
      '#value' => $this->t('PDF report'),
    ];

    $form['csv_report'] = [
      '#type' => 'button',
      '#value' => $this->t('CSV report'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement submitForm() method.
  }

}
