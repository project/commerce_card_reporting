<?php

namespace Drupal\commerce_card_reporting\Plugin\Commerce\ReportType;

use Drupal\commerce_price\Price;
use Drupal\commerce_reports\Plugin\Commerce\ReportType\ReportTypeBase;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the basic Card Report.
 *
 * @CommerceReportType(
 *   id = "card_report",
 *   label = @Translation("Card Report"),
 *   description = @Translation("Basic card report with card id, total, and created date")
 * )
 */
class CardReport extends ReportTypeBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];
    $fields['amount'] = BundleFieldDefinition::create('commerce_price')
      ->setLabel($this->t('Total Amount'))
      ->setDescription($this->t('The total amount of the order'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['code'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Response code'))
      ->setDescription($this->t('The response code.'))
      ->setDisplayConfigurable('view', TRUE);
    $fields['code_label'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Response code label'))
      ->setDescription($this->t('The response code label.'))
      ->setDisplayConfigurable('view', TRUE);
    $fields['description'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Description'))
      ->setDescription($this->t('The reason for payment error.'))
      ->setDisplayConfigurable('view', TRUE);
    $fields['card_type'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Credit Card Type'))
      ->setDescription($this->t('The credit card used for purchase.'))
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function generateReports(OrderInterface $order) {
    $values = [
      'amount' => $order->getTotalPrice(),
      'mail' => $order->getEmail(),
    ];
    $this->createFromOrder($order, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function generateCardReports(
    OrderInterface $order,
    array $error_details
  ) {
    $values = [
      'order_type_id' => $order->bundle(),
      'amount' => $order->getTotalPrice(),
      'code' => $error_details['code'],
      'code_label' => $error_details['code_label'],
      'description' => $error_details['code_description'],
      'card_type' => $error_details['card_type'],
      'created' => time(),
      'updated' => time(),
    ];
    $this->createFromOrder($order, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function buildQuery(QueryAggregateInterface $query) {
    $query->aggregate('amount.number', 'SUM');
    $query->aggregate('amount.number', 'AVG');
    $query->groupBy('amount.currency_code');
  }

  /**
   * {@inheritdoc}
   */
  protected function doBuildReportTableHeaders() {
    return [
      'formatted_date' => $this->t('Date'),
      'order_id_count' => $this->t('# Orders'),
      'amountnumber_sum' => $this->t('Total revenue'),
      'amountnumber_avg' => $this->t('Average revenue'),
      'amount_currency_code' => $this->t('Currency'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function doBuildReportTableRow(array $result) {
    $currency_code = $result['amount_currency_code'];
    $row = [
      $result['formatted_date'],
      $result['order_id_count'],
      $result['mail_count'],
      [
        'data' => [
          '#type' => 'inline_template',
          '#template' => '{{price|commerce_price_format}}',
          '#context' => [
            'price' => new Price($result['amountnumber_sum'], $currency_code),
          ],
        ],
      ],
      [
        'data' => [
          '#type' => 'inline_template',
          '#template' => '{{price|commerce_price_format}}',
          '#context' => [
            'price' => new Price($result['amountnumber_avg'], $currency_code),
          ],
        ],
      ],
      $currency_code,
    ];
    return $row;
  }

}
