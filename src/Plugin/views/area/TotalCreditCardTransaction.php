<?php

namespace Drupal\commerce_card_reporting\Plugin\views\area;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an total card transactions area handler.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("commerce_card_transactions_total")
 */
class TotalCreditCardTransaction extends AreaPluginBase {

  /**
   * The report storage.
   *
   * @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage
   */
  protected $reportStorage;

  /**
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Constructs a new OrderTotal instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->reportStorage = $entity_type_manager
      ->getStorage('commerce_order_report');
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $filter = [];

    if (!$empty || !empty($this->options['empty'])) {
      $query = $this->reportStorage
        ->getQuery()
        ->accessCheck(FALSE)
        ->condition('type', 'card_report');

      $date_from = $this->requestStack
        ->getCurrentRequest()->query->get('date_from');
      if ($date_from) {
        $filter['from'] = date("F j, Y", strtotime($date_from));
        $query->condition('created', strtotime($date_from), ">=");
      }

      $date_to = $this->requestStack
        ->getCurrentRequest()->query->get('date_to');
      if ($date_to) {
        $filter['to'] = date("F j, Y", strtotime($date_to));
        $query->condition('created', strtotime($date_to), "<=");
      }

      $count = $query->count()->execute();
      $text = '<div class="ccc-cc-transactions-header">
        <div class="ccc-cc-transactions-header__content">
          <div class="ccc-cc-transactions-header__title"><h3>' . $this->t('All cards') . '</h3></div>
          <div class="ccc-cc-transactions-header__number">
            <div class="ccc-cc-transactions-header__number__count">' . $count . '</div>
            <div class="ccc-cc-transactions-header__number__label">' . $this->t('total attempted transactions') . '</div>
          </div>
          <div class="ccc-cc-transactions-header__dates">' . implode(" - ", $filter) . '</div>
        </div>
      </div>';
      return [
        '#markup' => $text ,
      ];
    }

    return [];
  }

}
