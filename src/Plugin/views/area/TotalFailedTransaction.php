<?php

namespace Drupal\commerce_card_reporting\Plugin\views\area;

use Drupal\commerce_card_reporting\CardReportManager;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an total failed transactions area handler.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("commerce_card_total_failed_transactions")
 */
class TotalFailedTransaction extends AreaPluginBase {

  /**
   * The report storage.
   *
   * @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage
   */
  protected $reportStorage;

  /**
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * @var Drupal\commerce_card_reporting\CardReportManager
   */
  private $cardReportManager;

  /**
   * Constructs a new OrderTotal instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param Drupal\commerce_card_reporting\CardReportManager $card_report_manager
   *   The card report manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
    CardReportManager $card_report_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->reportStorage = $entity_type_manager
      ->getStorage('commerce_order_report');
    $this->requestStack = $request_stack;
    $this->cardReportManager = $card_report_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('commerce_card_reporting.card_report_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {
      $date_from = $this->requestStack
        ->getCurrentRequest()->query->get('date_from');
      $date_to = $this->requestStack
        ->getCurrentRequest()->query->get('date_to');

      $count = $this->cardReportManager
        ->totalFailedTransactions(
        $date_from,
        $date_to
      );
      $percentage = $this->cardReportManager
        ->getTotalTransactionPercentage(
          $count,
          $date_from,
          $date_to
        );

      $percentage_int = intval($percentage);
      $percentage_display = number_format($percentage, 2);

      $percentage_circle = '<div class="progress-circle progress-circle--failure progressing p-' . $percentage_int . '">
        <div class="progress-circle-border">
          <div class="left-half-circle"></div>
          <div class="right-half-circle"></div>
        </div>
      </div>';

      $label = $this->t('total failed transactions');
      $count = '<strong class="ccc-c-failure ccc-lg">' . $count . '</strong> ' . $label;

      $label = $this->t('Failed transactions not recovered by other cards');
      $transactions_value = '<span>' . $label
        . ' </span><br /><strong class="ccc-c-failure">125,000 (50%) = $937,500</strong>';

      $view_all_title = $this->t('view all');
      $view_all_title_active = $this->t('collapse list');
      $view_all_button = '<a href="#" class="js-ccc-card-report__list__view-all-button">
        <span class="js-ccc-card-report__list__view-all-button__default">' . $view_all_title . '</span>
        <span class="js-ccc-card-report__list__view-all-button__active">' . $view_all_title_active . '</span>
      </a>';

      $markup =
        <<<HTML
        <div class='ccc-card-report-row active'>
          <div class='ccc-card-report-row__details'>
            <div class='ccc-card-report-row__details-content'>
              <div class='ccc-card-report-row__percentage'>
                $percentage_circle
                <div class='ccc-card-report-row__percentage-value ccc-c-failure'>$percentage_display%</div>
                <div class='ccc-card-report-row__percentage-info'>
                  Overall<br />
                  failure rate
                </div>
              </div>
              <div class='ccc-card-report-row__details-col'>
                <div class='ccc-card-report-row__full-details ccc-card-report-row__full-details--offset'>$count</div>
                <div class='ccc-card-report-row__full-details'>
                  <div class='ccc-ico ccc-ico--failure ccc-ico--dollar'></div>
                  $transactions_value
                </div>
                <div class='ccc-card-report-row__full-details'>
                  <div class='ccc-ico ccc-ico--failure ccc-ico--refresh'></div>
                  Failed transactions recovered by other cards
                  <div><strong class='ccc-c-failure'>125,000 (50%) = $937,500</strong></div>
                </div>
                <div class='ccc-card-report-row__full-details'>
                  <div class='ccc-ico ccc-ico--failure ccc-ico--error'></div>
                  Error codes
                  <ul class='ccc-card-report__list js-ccc-card-report__list'>
                    <li>
                      05 - Do not honour <span class='ccc-c-failure'>x 35,866</span>
                    </li>
                    <li>
                      51 - Insufficient Funds <span class='ccc-c-failure'>x 35,866</span>
                    </li>
                    <li>
                      14 - Invalid Card Number <span class='ccc-c-failure'>x 30,866</span>
                    </li>
                    <li>
                      10 - Lorem Ipsum Dolor <span class='ccc-c-failure'>x 15,866</span>
                    </li>
                    <li>
                      22 - Lorem Ipsum Dolor <span class='ccc-c-failure'>x 35,866</span>
                    </li>
                    <li>
                      88 - Lorem Ipsum Dolor <span class='ccc-c-failure'>x 45,866</span>
                    </li>
                    <li>
                      77 - Lorem Ipsum Dolor <span class='ccc-c-failure'>x 11,466</span>
                    </li>
                    <li>
                      99 - Lorem Ipsum Dolor <span class='ccc-c-failure'>x 5,866</span>
                    </li>
                    <li class='ccc-card-report__list__view-all'>
                      $view_all_button
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        HTML;

      return ['#markup' => $markup];
    }

    return [];
  }

}
