<?php

namespace Drupal\commerce_card_reporting\Plugin\views\area;

use Drupal\commerce_card_reporting\CardReportManager;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an total failed transactions area handler.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("commerce_card_total_successful_transactions")
 */
class TotalSuccessfulTransactions extends AreaPluginBase {

  /**
   * The report storage.
   *
   * @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage
   */
  protected $reportStorage;

  /**
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * @var Drupal\commerce_card_reporting\CardReportManager
   */
  private $cardReportManager;

  /**
   * Constructs a new OrderTotal instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param Drupal\commerce_card_reporting\CardReportManager $card_report_manager
   *   The card report manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
    CardReportManager $card_report_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->reportStorage = $entity_type_manager
      ->getStorage('commerce_order_report');
    $this->requestStack = $request_stack;
    $this->cardReportManager = $card_report_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('commerce_card_reporting.card_report_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {
      $date_from = $this->requestStack
        ->getCurrentRequest()->query->get('date_from');
      $date_to = $this->requestStack
        ->getCurrentRequest()->query->get('date_to');

      $count = $this->cardReportManager
        ->totalCompletedTransactions(
          $date_from,
          $date_to
        );
      $percentage = $this->cardReportManager
        ->getTotalTransactionPercentage(
          $count,
          $date_from,
          $date_to
        );

      $percentage_int = intval($percentage);
      $percentage_display = number_format($percentage, 2);

      $percentage_circle = '<div class="progress-circle progress-circle--success progressing p-' . $percentage_int . '">
        <div class="progress-circle-border">
          <div class="left-half-circle"></div>
          <div class="right-half-circle"></div>
        </div>
      </div>';

      $label = (string) $this->t('total completed transactions');
      $count = '<strong class="ccc-c-success ccc-lg">' . $count . '</strong> ' . $label;

      $label = (string) $this->t('Total transactions value');
      $transactions_value = '<span>' . $label
        . ' </span><br /><strong class="ccc-c-success">$7,500,000.00</strong>';

      $markup =
        <<<HTML
        <div class='ccc-card-report-row active'>
          <div class='ccc-card-report-row__details'>
            <div class='ccc-card-report-row__details-content'>
              <div class='ccc-card-report-row__percentage'>
                $percentage_circle
                <div class='ccc-card-report-row__percentage-value ccc-c-success'>$percentage_display%</div>
                <div class='ccc-card-report-row__percentage-info'>
                  Overall<br />
                  success rate
                </div>
              </div>
              <div class='ccc-card-report-row__details-col'>
                <div class='ccc-card-report-row__full-details ccc-card-report-row__full-details--offset'>$count</div>
                <div class='ccc-card-report-row__full-details'>
                  <div class='ccc-ico ccc-ico--success ccc-ico--dollar'></div>
                  $transactions_value
                </div>
                <div class='ccc-card-report-row__full-details'>
                  <div class='ccc-ico ccc-ico--success ccc-ico--refresh'></div>
                  Transactions recovered by other cards
                  <div><strong class='ccc-c-success'>125,000 (50%) = $937,500</strong></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        HTML;

      return ['#markup' => $markup];
    }

    return [];
  }

}
