<?php

namespace Drupal\commerce_card_reporting\Plugin\views\field;

use Drupal\commerce_card_reporting\CardReportManager;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;

use Drupal\Core\Entity\EntityTypeManagerInterface;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to display the completed transactions details of a credit card.
 *
 * Displays the total completed transactions by credit card, also displays the
 * percentage of transactions by that particular credit card and total
 * transactions value.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("commerce_card_reporting_total_completed_transactions")
 */
class TotalCompletedTransactions extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * @var Drupal\commerce_card_reporting\CardReportManager
   */
  private $cardReportManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
    CardReportManager $card_report_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->cardReportManager = $card_report_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('commerce_card_reporting.card_report_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $card_type = $values->_entity->get('card_type')->value;
    $card_type = preg_replace("/[^A-Za-z0-9]/", '', $card_type);

    if (!$card_type) {
      return;
    }

    $date_from = $this->requestStack
      ->getCurrentRequest()->query->get('date_from');
    $date_to = $this->requestStack
      ->getCurrentRequest()->query->get('date_to');

    $count = $this->cardReportManager->totalCompletedTransactions(
      $date_from,
      $date_to,
      $card_type
    );
    $percentage = round(
      $this->cardReportManager
        ->getTotalTransactionPercentage(
          $count,
          $date_from,
          $date_to
        )
    );

    $percentage_int = intval($percentage);

    $percentage_circle = '<div class="progress-circle progress-circle--success progressing p-' . $percentage_int . '">
      <div class="progress-circle-border">
        <div class="left-half-circle"></div>
        <div class="right-half-circle"></div>
      </div>
    </div>';

    $label = $this->t('completed [Card] transactions');
    $count = '<strong class="ccc-c-success ccc-lg">' . $count . '</strong> ' . $label;

    $payment_details = $this->totalCompletedTransactionValue($card_type);
    $label = $this->t('total [Card] transactions value');
    $transactions_value = '<span>' . $label
      . ' </span><br /><strong class="ccc-c-success">' . $payment_details['total_value'] . '</strong>';

    $markup =
      <<<HTML
      <div class='ccc-card-report-row__details'>
        <div class='ccc-card-report-row__details-content'>
          <div class='ccc-card-report-row__percentage'>
            $percentage_circle
            <div class='ccc-card-report-row__percentage-value ccc-c-success'>$percentage%</div>
            <div class='ccc-card-report-row__percentage-info'>
              [Card]<br />
              success rate
            </div>
          </div>
          <div class='ccc-card-report-row__details-col'>
            <div class='ccc-card-report-row__full-details ccc-card-report-row__full-details--offset'>$count</div>
            <div class='ccc-card-report-row__full-details'>
              <div class='ccc-ico ccc-ico--success ccc-ico--dollar'></div>
              $transactions_value
            </div>
            <div class='ccc-card-report-row__full-details'>
              <div class='ccc-ico ccc-ico--success ccc-ico--refresh'></div>
              Failed transactions recovered by [Card] cards
              <div><strong class='ccc-c-success'>62,500 (50%) = $468,750</strong></div>
              <ul class='ccc-card-report__list'>
                <li>Recovered <span class='ccc-c-success'>20,000</span> failed AMEX transactions</li>
                <li>Recovered <span class='ccc-c-success'>15,000</span> failed Discover transactions</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      HTML;

    return ['#markup' => $markup];
  }

  /**
   * Provides the total completed transactions value of a credit card type.
   *
   * @param string $card_type
   *   The type of the card whose transactions are to be fetched.
   *
   * @return array
   *   An array with total value of transactions.
   */
  private function totalCompletedTransactionValue($card_type) {
    $output = ['total_value' => NULL];
    $payment_storage = $this->entityTypeManager
      ->getStorage('commerce_payment');

    $payments = $payment_storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('state', 'completed')
      ->condition('payment_method.entity.type', 'credit_card')
      ->condition('payment_method.entity.card_type', $card_type)
      ->execute();

    $payments = $payment_storage->loadMultiple($payments);
    foreach ($payments as $payment) {
      if (isset($output['total_value'])) {
        $output['total_value']->add($payment->getAmount());
      }
      else {
        $output['total_value'] = $payment->getAmount();
      }
    }

    return $output;
  }

}
