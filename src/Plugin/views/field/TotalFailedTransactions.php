<?php

namespace Drupal\commerce_card_reporting\Plugin\views\field;

use Drupal\commerce_card_reporting\CardReportManager;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;

use Drupal\Core\Entity\EntityTypeManagerInterface;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to display the failed transactions details of a credit card.
 *
 * Displays the total failed transactions by credit card, also displays the
 * percentage of transactions by that particular credit card and total
 * transactions value.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("commerce_card_reporting_total_failed_transactions")
 */
class TotalFailedTransactions extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * @var Drupal\commerce_card_reporting\CardReportManager
   */
  private $cardReportManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
    CardReportManager $card_report_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->cardReportManager = $card_report_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('commerce_card_reporting.card_report_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $card_type = $values->_entity->get('card_type')->value;

    if (!$card_type) {
      return;
    }

    $date_from = $this->requestStack
      ->getCurrentRequest()->query->get('date_from');
    $date_to = $this->requestStack
      ->getCurrentRequest()->query->get('date_to');
    $count = $this->cardReportManager->totalFailedTransactions(
      $date_from,
      $date_to,
      $card_type
    );
    $percentage = round(
      $this->cardReportManager
        ->getTotalTransactionPercentage(
          $count,
          $date_from,
          $date_to
        )
    );

    $percentage_int = intval($percentage);

    $percentage_circle = '<div class="progress-circle progress-circle--failure progressing p-' . $percentage_int . '">
      <div class="progress-circle-border">
        <div class="left-half-circle"></div>
        <div class="right-half-circle"></div>
      </div>
    </div>';

    $label = $this->t('total failed transactions');
    $count = '<strong class="ccc-c-failure ccc-lg">' . $count . '</strong> ' . $label;

    $payment_details = $this->totalFailedTransactionValue($card_type);
    $label = $this->t('Failed transactions not recovered by other cards');
    $transactions_value = '<span>' . $label
      . ' </span><br /><strong class="ccc-c-failure">' . $payment_details['total_value'] . '</strong>';

    $view_details_title = $this->t('Toggle Details');
    $view_details_button = '<a href="#" class="js-ccc-view-row-details" aria-label="' . $view_details_title . '" title="' . $view_details_title . '">' . $view_details_title . '</a>';

    $view_all_title = $this->t('view all');
    $view_all_title_active = $this->t('collapse list');
    $view_all_button = '<a href="#" class="js-ccc-card-report__list__view-all-button">
      <span class="js-ccc-card-report__list__view-all-button__default">' . $view_all_title . '</span>
      <span class="js-ccc-card-report__list__view-all-button__active">' . $view_all_title_active . '</span>
    </a>';

    $markup =
      <<<HTML
      <div class='ccc-card-report-row__details'>
        <div class='ccc-card-report-row__details-content'>
          <div class='ccc-card-report-row__percentage'>
            $percentage_circle
            <div class='ccc-card-report-row__percentage-value ccc-c-failure'>$percentage%</div>
            <div class='ccc-card-report-row__percentage-info'>
              [Card]<br />
              failure rate
            </div>
            $view_details_button
          </div>
          <div class='ccc-card-report-row__details-col'>
            <div class='ccc-card-report-row__full-details ccc-card-report-row__full-details--offset'>$count</div>
            <div class='ccc-card-report-row__full-details'>
              <div class='ccc-ico ccc-ico--failure ccc-ico--dollar'></div>
              $transactions_value
            </div>
            <div class='ccc-card-report-row__full-details'>
              <div class='ccc-ico ccc-ico--failure ccc-ico--refresh'></div>
              Failed [Card] transactions recovered by other cards
              <div><strong class='ccc-c-failure'>62,500 (50%) = $468,750</strong></div>
              <ul class='ccc-card-report__list'>
                <li><span class='ccc-c-failure'>20,000</span> failed Visa transactions recovered by Mastercard</li>
                <li><span class='ccc-c-failure'>15,000</span> failed Visa transactions recovered by Discover</li>
              </ul>
            </div>
            <div class='ccc-card-report-row__full-details'>
              <div class='ccc-ico ccc-ico--failure ccc-ico--error'></div>
                Error codes
                <ul class='ccc-card-report__list js-ccc-card-report__list'>
                  <li>
                    05 - Do not honour <span class='ccc-c-failure'>x 35,866</span>
                  </li>
                  <li>
                    51 - Insufficient Funds <span class='ccc-c-failure'>x 35,866</span>
                  </li>
                  <li>
                    14 - Invalid Card Number <span class='ccc-c-failure'>x 30,866</span>
                  </li>
                  <li>
                    10 - Lorem Ipsum Dolor <span class='ccc-c-failure'>x 15,866</span>
                  </li>
                  <li>
                    22 - Lorem Ipsum Dolor <span class='ccc-c-failure'>x 35,866</span>
                  </li>
                  <li>
                    88 - Lorem Ipsum Dolor <span class='ccc-c-failure'>x 45,866</span>
                  </li>
                  <li>
                    77 - Lorem Ipsum Dolor <span class='ccc-c-failure'>x 11,466</span>
                  </li>
                  <li>
                    99 - Lorem Ipsum Dolor <span class='ccc-c-failure'>x 5,866</span>
                  </li>
                  <li class='ccc-card-report__list__view-all'>
                    $view_all_button
                  </li>
                </ul>
            </div>
          </div>
        </div>
      </div>
      HTML;

    return ['#markup' => $markup];
  }

  /**
   * Provides the total failed transactions value of a credit card type.
   *
   * @param string $card_type
   *   The type of the card whose transactions are to be fetched.
   *
   * @return array
   *   An array with total value of transactions.
   */
  private function totalFailedTransactionValue($card_type) {
    $output = [];
    $report_storage = $this->entityTypeManager
      ->getStorage('commerce_order_report');

    $card_reports = $report_storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'card_report')
      ->condition('card_type', $card_type)
      ->execute();

    $card_reports = $report_storage->loadMultiple($card_reports);
    foreach ($card_reports as $card_report) {
      if (isset($output['total_value'])) {
        $output['total_value']->add($card_report->amount->first()->toPrice());
      }
      else {
        $output['total_value'] = $card_report->amount->first()->toPrice();
      }
    }

    return $output;
  }

}
