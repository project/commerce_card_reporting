<?php

namespace Drupal\Tests\commerce_card_reporting\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for Commerce Card reporting.
 *
 * @group commerce_card_reporting
 */
class GenericTest extends GenericModuleTestBase {

  /**
   * @todo enable this.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function assertHookHelp(string $module): void {
    // @todo Remove this override.
  }

}
